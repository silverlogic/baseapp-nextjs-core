import { checkPermissions } from './utils'
import { UserWithPermissions, PermissionOptions } from './types'

interface IProps {
  user?: object
}

export const withPermissions = (
  Component: React.JSXElementConstructor<any>,
  options: PermissionOptions = {},
) => {
  const { all, any, PermissionDeniedComponent, hide } = options

  return (props: IProps) => {
    const { user } = props
    const NotEnoughPermissions =
      PermissionDeniedComponent ??
      (() => <p>You don't have enough permissions to access this page!</p>)

    if (!checkPermissions(user as UserWithPermissions, any, all)) {
      return hide ? null : <NotEnoughPermissions />
    }

    return <Component {...props} />
  }
}
